var puppeteer = require('puppeteer');
var fs = require("fs");

extractData = () => {
    const extractedElements = document.querySelectorAll(`span.name, span.price`);
    const items = [];
    for (let element of extractedElements) {
        items.push(element.innerText);
    }
    return items;
}

getItems = async (page) => {
    let items = [];
    let scrollDelay = 2000;
    try{
        let prevHeight;
        while(true) {
            prevHeight = await page.evaluate('document.body.scrollHeight');
            items = await page.evaluate(extractData);
            await page.evaluate(`window.scrollTo(0, ${prevHeight})`);
            await page.waitForFunction(`document.body.scrollHeight > ${prevHeight}`);
            await page.waitFor(scrollDelay);
            newHeight = await page.evaluate('document.body.scrollHeight');
            if(newHeight===prevHeight){
                break;
            }
        }
    } catch (e) {
        console.log(e);
    }
    return items;
}

start = async () => {

    // Starting the browser
    const browser = await puppeteer.launch({
            headless: false,
            args: ['--no-sandbox', '--disable-setuid-sandbox'],
    });

    const page = await browser.newPage();
    page.setViewport({ width: 1280, height: 926 });

    await page.goto('https://www.diffmarts.com/search/milk');

    let items = await getItems(page);

    fs.writeFileSync('./data/milkdata.csv', items.join(',') + '\n')

    browser.close();
}

start();